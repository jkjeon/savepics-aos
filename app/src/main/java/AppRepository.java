import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.wishpoke.savepics.model.AppStorage;

import java.util.ArrayList;

public class AppRepository {
    private AppStorage storage;
    private MutableLiveData<ArrayList<String>> recentData;

    public AppRepository(Application application) {
        if (recentData == null) {
            recentData = new MutableLiveData<>();
        }
        storage = new AppStorage(application, recentData);
    }

    public MutableLiveData<ArrayList<String>> getRecent() {
        return recentData;
    }

    public void addRecent(String url) {
        storage.add(url);
    }

    public void removeRecent(String url) {
        storage.remove(url);
    }
}
