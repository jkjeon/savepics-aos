package com.wishpoke.savepics.model;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

public class AppStorage {
    private SharedPreferences pref;

    private String RECENT = "recent";
    private String IS_FIRST = "isFirst";
    private String PURCHASED_REMOVE_ADS = "remove_ads";
    private String IG_COOKIES = "ig_cookies";
    private String QUIT_AT = "quit_at"; // 종료시 기록

    private final int MAX = 5;
    private final String divider = "m789m";
    private final int RECONNECTION_MINUTES = 1;

    private MutableLiveData<ArrayList<String>> recentData;

    public AppStorage(Context context, MutableLiveData<ArrayList<String>> recentData) {
        pref = context.getSharedPreferences("sp_recent_storage", Context.MODE_PRIVATE);
        this.recentData = recentData;
        this.recentData.setValue(getArrayList());
    }

    public AppStorage(Context context) {
        pref = context.getSharedPreferences("sp_recent_storage", Context.MODE_PRIVATE);
    }

    public boolean isFirstLaunch() {
        boolean isFirst = pref.getBoolean(IS_FIRST, true);
        if (isFirst) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(IS_FIRST, false);
            editor.apply();
        }
        return isFirst;
    }

    public void setQuitAt() {
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(QUIT_AT, (System.currentTimeMillis() / 1000));
        editor.apply();
    }

    public boolean isReconnection() {
        boolean isReconnection = false;
        long now = System.currentTimeMillis() / 1000;
        long quitAt = pref.getLong(QUIT_AT, (System.currentTimeMillis() / 1000));
        if (now - quitAt > RECONNECTION_MINUTES * 60) {
            isReconnection = true;
        }
        return isReconnection;
    }

    public boolean purchasedRemoveAds() {
        return pref.getBoolean(PURCHASED_REMOVE_ADS, false);
    }

    public void setPurchasedRemoveAds(boolean flag) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(PURCHASED_REMOVE_ADS, flag);
        editor.apply();
    }

    public void setIgCookies(String cookies) {
        if (cookies.contains("ds_user_id")) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(IG_COOKIES, cookies);
            editor.apply();
        }
    }

    public String getIgCookies() {
        return pref.getString(IG_COOKIES, "");
    }

    private ArrayList<String> getArrayList() {
        String rawString = pref.getString(RECENT, "");
        ArrayList<String> arrayList = new ArrayList<>();
        if (!rawString.equals("")) {
            String[] stringArr = rawString.split(divider);
            arrayList.addAll(Arrays.asList(stringArr));
        }
        return arrayList;
    }

    private String arrayToString(ArrayList<String> arrayList) {
        // ArrayList -> String
        String rawString = "";
        for (int i = 0; i < arrayList.size(); i++) {
            rawString += arrayList.get(i);
            if (i != arrayList.size() - 1) {
                rawString += divider;
            }
        }
        return rawString;
    }

    public void remove(String url) {
        ArrayList<String> arrayList = getArrayList();
        arrayList.remove(url);

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(RECENT, arrayToString(arrayList));
        editor.apply();

        recentData.setValue(getArrayList());
    }

    public void add(String url) {
        ArrayList<String> arrayList = getArrayList();
        // 가장 최근 것이 중복이 아닌경우 추가
        if (arrayList.size() == 0 || !arrayList.get(arrayList.size() - 1).equals(url)) {
            if (arrayList.size() == MAX) {
                arrayList.remove(0);
            }
            arrayList.add(url);
        }

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(RECENT, arrayToString(arrayList));
        editor.apply();

        recentData.setValue(getArrayList());
    }
}
