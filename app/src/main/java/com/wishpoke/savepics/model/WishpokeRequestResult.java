package com.wishpoke.savepics.model;

import java.util.ArrayList;

public class WishpokeRequestResult {
    public Status status;
    public ArrayList<Result> results;

    public class Status {
        public int code;
        public String msg;
    }

    public class Result {
        public String type;
        public String content;
    }
}

