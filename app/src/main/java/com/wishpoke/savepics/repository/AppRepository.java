package com.wishpoke.savepics.repository;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.util.ImageResolver;
import com.wishpoke.savepics.util.MyCallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class AppRepository {
    private AppStorage storage;
    private MutableLiveData<ArrayList<String>> recentData;
    private Application application;
    int countSuccess = 0;

    public AppRepository(Application application) {
        this.application = application;
        if (recentData == null) {
            recentData = new MutableLiveData<>();
        }
        storage = new AppStorage(application, recentData);
    }

    public MutableLiveData<ArrayList<String>> getRecent() {
        return recentData;
    }

    public void addRecent(String url) {
        storage.add(url);
    }

    public void removeRecent(String url) {
        storage.remove(url);
    }

    @SuppressLint("CheckResult")
    public void saveImages(final ArrayList<String> imageUrls, final MyCallback<ArrayList<Uri>> callback) {
        final ArrayList<Uri> savedFiles = new ArrayList<>();
        for (String url : imageUrls) {
            Glide.with(application).asBitmap().load(url).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    File file = ImageResolver.saveBitmap(resource);
                    ImageResolver.addImageGallery(application, file);
                    String filePath = file.getPath();
                    savedFiles.add(Uri.parse(filePath));
                    if (savedFiles.size() == imageUrls.size()) {
                        callback.done(savedFiles);
                    }
                }
            });
        }
    }
}
