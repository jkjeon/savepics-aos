package com.wishpoke.savepics.repository;

import com.wishpoke.savepics.model.WishpokeRequestResult;
import com.wishpoke.savepics.service.WishpokeAPIService;

import java.util.HashMap;
import java.util.Locale;

import io.reactivex.Single;

public class SettingsRepository {
    public SettingsRepository() {

    }

    public Single<WishpokeRequestResult> getTerms() {
        String localeCode = Locale.getDefault().getLanguage();
        localeCode = localeCode.equals("ko") ? localeCode : "en";

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("lang_cd", localeCode);
        return WishpokeAPIService.getAPI().getTerms(WishpokeAPIService.getRequestBodyByKeyValuePair(hashMap));
    }
}