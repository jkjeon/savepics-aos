package com.wishpoke.savepics.view;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.wishpoke.savepics.BuildConfig;
import com.wishpoke.savepics.R;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.resource.SpacesItemDecoration;
import com.wishpoke.savepics.util.Animator;
import com.wishpoke.savepics.util.ImageResolver;
import com.wishpoke.savepics.util.MyCallback;
import com.wishpoke.savepics.view.adapter.PhotoAdapter;
import com.wishpoke.savepics.viewmodel.SelectPhotoViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectPhotoActivity extends AppCompatActivity {

    @BindView(R.id.btn_save)
    LinearLayout btnSave;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.tv_save)
    TextView tvSave;
    @BindView(R.id.rv_photo)
    RecyclerView rvPhoto;
    @BindView(R.id.ic_loading)
    ImageView icLoading;
    @BindView(R.id.nested_scrollview)
    NestedScrollView scrollView;
    @BindView(R.id.adView)
    AdView adView;

    private ArrayList<String> imageUrls = new ArrayList<>();
    private PhotoAdapter adapter;
    private Context context;
    private SelectPhotoViewModel viewModel;

    private static final int REQUEST_EXTERNAL_STORAGE = 1710;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private AppStorage storage;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_photo);
        ButterKnife.bind(this);
        storage = new AppStorage(this);
        context = this;

        loadAds();

        imageUrls = getIntent().getStringArrayListExtra("images");
        viewModel = ViewModelProviders.of(this).get(SelectPhotoViewModel.class);

        adapter = new PhotoAdapter(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = (String) v.getTag();
                if (!adapter.getSelectedImageUrls().contains(url)) {
                    v.findViewById(R.id.ll_select).setVisibility(View.VISIBLE);
                    adapter.addSelectedImageUrl(url);
                } else {
                    v.findViewById(R.id.ll_select).setVisibility(View.INVISIBLE);
                    adapter.removeSelectedImageUrl(url);
                }
                boolean saveEnabled = adapter.getSelectedImageUrls().size() > 0;
                int buttonBgRes = saveEnabled ? R.drawable.background_button_search : R.drawable.background_button_search_disabled;
                btnSave.setEnabled(saveEnabled);
                btnSave.setBackground(ContextCompat.getDrawable(context, buttonBgRes));
            }
        });
        adapter.setImageUrls(imageUrls);

        rvPhoto.setLayoutManager(new GridLayoutManager(this, 3));
        rvPhoto.setHasFixedSize(true);
        rvPhoto.setNestedScrollingEnabled(false);
        rvPhoto.addItemDecoration(new SpacesItemDecoration((int) ImageResolver.convertDpToPixel(1, this)));
        rvPhoto.setAdapter(adapter);

        scrollView.setSmoothScrollingEnabled(true);
        scrollView.fullScroll(View.FOCUS_UP);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSave.startAnimation(Animator.getClickAnimation(context));
                if (verifyStoragePermissions(SelectPhotoActivity.this)) {
                    final ArrayList<String> imageUrls = adapter.getSelectedImageUrls();
                    viewModel.saveImages(imageUrls, new MyCallback<ArrayList<Uri>> () {
                        @Override
                        public void done(ArrayList<Uri> savedUris) {
                            icLoading.animate().alpha(0).setDuration(500).start();
                            icLoading.clearAnimation();
                            Intent intent = new Intent(context, SaveCompleteActivity.class);
                            intent.putExtra("images", imageUrls);
                            intent.putExtra("uris", savedUris);
                            startActivity(intent);
                            finish();

                        }
                    });
                    icLoading.animate().alpha(1).setDuration(500).start();
                    icLoading.startAnimation(Animator.getLoadingAnimation(context));
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnBack.startAnimation(Animator.getClickAnimation(context));
                onBackPressed();
            }
        });
    }

    private void loadAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        adView.loadAd(adRequest);
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public boolean verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onPause() {
        storage.setQuitAt();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (storage.isReconnection() && !storage.purchasedRemoveAds()) {
            interstitialAd.show();
        }
        super.onResume();
    }
}
