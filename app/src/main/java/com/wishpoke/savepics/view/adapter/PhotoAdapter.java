package com.wishpoke.savepics.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wishpoke.savepics.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    private ArrayList<String> imageUrls = new ArrayList<>();
    private ArrayList<String> selectImageUrls = new ArrayList<>();
    private Context context;
    private View.OnClickListener clickListener;

    public PhotoAdapter(Context context, View.OnClickListener clickListener) {
        this.context = context;
        this.clickListener = clickListener;
    }

    public void setImageUrls(ArrayList<String> imageUrls) {
        this.imageUrls = imageUrls;
        notifyDataSetChanged();
    }

    public void addSelectedImageUrl(String url) {
        selectImageUrls.add(url);
    }

    public void removeSelectedImageUrl(String url) {
        selectImageUrls.remove(url);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.view.setOnClickListener(clickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final String url = imageUrls.get(position);
        Glide.with(context).load(url).into(holder.ivPhoto);
        holder.view.setTag(url);
        if (selectImageUrls.contains(url)) {
            holder.llSelect.setVisibility(View.VISIBLE);
        } else {
            holder.llSelect.setVisibility(View.INVISIBLE);
        }
    }

    public ArrayList<String> getSelectedImageUrls() {
        return selectImageUrls;
    }

    @Override
    public int getItemCount() {
        return imageUrls.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_photo)
        ImageView ivPhoto;
        @BindView(R.id.ll_select)
        LinearLayout llSelect;
        public View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
