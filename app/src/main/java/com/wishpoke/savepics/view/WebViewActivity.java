package com.wishpoke.savepics.view;

import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.wishpoke.savepics.R;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.util.Animator;
import com.wishpoke.savepics.util.Validator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends AppCompatActivity {
    @BindView(R.id.btn_cancel)
    ImageView btnCancel;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.btn_forward)
    ImageView btnForward;
    @BindView(R.id.et_url)
    EditText etUrl;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.btn_save)
    LinearLayout btnSave;
    @BindView(R.id.tv_save)
    TextView tvSave;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.ll_tools)
    LinearLayout llTools;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    private String url;
    private String cacheUrl;
    private Context context;
    private AppStorage storage;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        storage = new AppStorage(this);
        context = this;

        loadAds();

        url = getIntent().getStringExtra("url");
        etUrl.setText(url);

        setWebView();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(context));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(context));
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(context));
                if (webView.canGoForward()) {
                    webView.goForward();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUrl.clearFocus();
                etUrl.setText(cacheUrl);
                viewRevealTools();
            }
        });

        etUrl.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    viewRevealCancel();
                    cacheUrl = etUrl.getText().toString();
                } else {
                    viewRevealTools();
                }
            }
        });
        etUrl.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    url = etUrl.getText().toString();
                    loadUrl();
                    return true;
                }
                return false;
            }
        });

        btnSave.setOnClickListener(saveClickListener);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView() {
        webView.setWebChromeClient(chromeClient);
        webView.setWebViewClient(webViewClient);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setAllowFileAccess(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(webView, true);
        }

        CookieSyncManager.createInstance(webView.getContext());
        CookieSyncManager.getInstance().startSync();

        loadUrl();
    }

    private WebChromeClient chromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (pbLoading.getAlpha() == 0) {
                pbLoading.animate().alpha(1).setDuration(300).start();
            }
            pbLoading.setProgress(newProgress);
        }
    };

    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onPageFinished(WebView view, final String url) {
            super.onPageFinished(view, url);
            String script = "document.getElementsByClassName('TQUPK')[0] != null ? 1 : 0";
            webView.evaluateJavascript(script, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    if (url.contains("instagram") && value.equals("1") && CookieManager.getInstance().getCookie(url).contains("ds_user_id")) {
                        webView.reload();
                    }
                }
            });
            if (pbLoading.getAlpha() == 1) {
                pbLoading.animate().alpha(0).setDuration(300).start();
                pbLoading.setProgress(0);
            }
        }
    };

    private View.OnClickListener saveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String script = "var imgs = []; for (var i = 0; i < document.images.length; i++) {imgs.push(document.images[i].src); imgs.toString();}";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                webView.evaluateJavascript(script, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        String[] images = value.replace("\"", "").split(",");
                        ArrayList<String> imageUrls = new ArrayList<>();
                        for(int i = 0; i < images.length; i++) {
                            if(images[i].startsWith("http") && !images[i].contains(".svg")) {
                                if(!imageUrls.contains(images[i])) {
                                    imageUrls.add(images[i]);
                                }
                            }
                        }
                        if (imageUrls.size() > 0) {
                            Intent intent = new Intent(context, SelectPhotoActivity.class);
                            intent.putExtra("images", imageUrls);
                            startActivity(intent);
                        } else {
                            Toast.makeText(context, R.string.web_image_not_found, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
    };

    private void loadUrl() {
        url = Validator.validateUrl(url);
        etUrl.setText(url);
        webView.loadUrl(url);
    }

    private void viewRevealTools() {
        llTools.clearAnimation();
        tvCancel.clearAnimation();

        llTools.setAlpha(0f);
        llTools.setVisibility(View.VISIBLE);
        llTools.animate().alpha(1f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                llTools.setVisibility(View.VISIBLE);
            }
        }).start();

        tvCancel.setAlpha(1f);
        tvCancel.animate().alpha(0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                tvCancel.setVisibility(View.GONE);
            }
        }).start();
    }

    private void viewRevealCancel() {
        llTools.clearAnimation();
        tvCancel.clearAnimation();

        tvCancel.setAlpha(0f);
        tvCancel.setVisibility(View.VISIBLE);
        tvCancel.animate().alpha(1f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                tvCancel.setVisibility(View.VISIBLE);
            }
        }).start();

        llTools.setAlpha(1f);
        llTools.animate().alpha(0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                llTools.setVisibility(View.GONE);
            }
        }).start();
    }

    private void loadAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    @Override
    protected void onPause() {
        storage.setQuitAt();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (storage.isReconnection() && !storage.purchasedRemoveAds()) {
            interstitialAd.show();
        }
        super.onResume();
    }
}
