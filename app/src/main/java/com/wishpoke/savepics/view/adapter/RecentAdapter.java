package com.wishpoke.savepics.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wishpoke.savepics.R;
import com.wishpoke.savepics.viewmodel.PasteUrlViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentAdapter extends RecyclerView.Adapter<RecentAdapter.ViewHolder> {

    private Context context;
    private PasteUrlViewModel viewModel;
    private ArrayList<String> urls;
    private View.OnClickListener urlClickListener;

    public RecentAdapter(PasteUrlViewModel viewModel, View.OnClickListener urlClickListener) {
        this.viewModel = viewModel;
        this.urlClickListener = urlClickListener;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recent, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final String url = urls.get(position);
        holder.tvUrl.setText(url);
        holder.tvUrl.setTag(url);
        holder.tvUrl.setOnClickListener(urlClickListener);
        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.removeRecent(url);
            }
        });
    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btn_remove)
        ImageView btnRemove;
        @BindView(R.id.tv_url)
        TextView tvUrl;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
