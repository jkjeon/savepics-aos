package com.wishpoke.savepics.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.wishpoke.savepics.R;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.resource.Config;
import com.wishpoke.savepics.util.Animator;

import butterknife.BindView;
import butterknife.ButterKnife;
import spencerstudios.com.bungeelib.Bungee;

public class FeedbackActivity extends AppCompatActivity {

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.adView)
    AdView adView;

    private Context context;
    private AppStorage storage;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        storage = new AppStorage(this);
        context = this;

        loadAds();
        setWebView();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(context));
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                        if (!webView.canGoBack()) {
                            Bungee.slideRight(context);
                        }
                    }
                }, 100);
            }
        });
    }

    private void loadAds() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(Config.FEEDBACK_URL);
    }

    @Override
    protected void onPause() {
        storage.setQuitAt();
        super.onPause();
    }

    @Override
    protected void onResume() {
        toggleAds();
        if (storage.isReconnection() && !storage.purchasedRemoveAds()) {
            interstitialAd.show();
        }
        super.onResume();
    }

    private void toggleAds() {
        if (storage.purchasedRemoveAds()) {
            adView.setVisibility(View.GONE);
        } else {
            adView.setVisibility(View.VISIBLE);
        }
    }
}
