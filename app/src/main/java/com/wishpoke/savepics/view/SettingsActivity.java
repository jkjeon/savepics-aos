package com.wishpoke.savepics.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.Constants;
import com.anjlab.android.iab.v3.PurchaseState;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.wishpoke.savepics.R;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.model.WishpokeRequestResult;
import com.wishpoke.savepics.resource.Config;
import com.wishpoke.savepics.util.Animator;
import com.wishpoke.savepics.viewmodel.SettingsViewModel;

import org.sufficientlysecure.htmltextview.HtmlAssetsImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class SettingsActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {

    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.tv_feedback)
    TextView tvFeedback;
    @BindView(R.id.tv_share)
    TextView tvShare;
    @BindView(R.id.tv_review)
    TextView tvReview;
    @BindView(R.id.tv_no_ads)
    TextView tvNoAds;
    @BindView(R.id.tv_terms)
    TextView tvTerms;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.tv_wishpoke)
    TextView tvWishpoke;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    private Context context;
    private AlertDialog dialog;
    private View dialogView;
    private AppStorage storage;
    private InterstitialAd interstitialAd;
    private SettingsViewModel viewModel;
    private BillingProcessor bp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);
        bp = new BillingProcessor(this, Config.GP_LICENSE_KEY, this);
        bp.initialize();
        storage = new AppStorage(this);
        context = this;

        animationView.setImageAssetsFolder("images/");
        animationView.setAnimation("background.json");
        animationView.playAnimation();

        loadAds();

        viewModel.getTerms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<WishpokeRequestResult>() {
                    @Override
                    public void accept(WishpokeRequestResult wishpokeRequestResult) throws Exception {
                        if (wishpokeRequestResult.status.code == 0) {
                            String content = wishpokeRequestResult.results.get(1).content;
                            loadTerms(content);
                        }
                    }
                });

        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionText = getString(R.string.settings_version) + " " + pInfo.versionName;
            tvVersion.setText(versionText);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tvFeedback.setOnClickListener(feedbackListener);
        tvShare.setOnClickListener(shareListener);
        tvReview.setOnClickListener(reviewListener);
        tvNoAds.setOnClickListener(noAdsListener);
        tvTerms.setOnClickListener(termsListener);
        tvWishpoke.setOnClickListener(wishpokeListener);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(context));
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                        Bungee.slideRight(context);
                    }
                }, 100);
            }
        });
    }

    private void loadAds() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }


    private void loadTerms(String content) {
        dialogView = getLayoutInflater().inflate(R.layout.dialog_terms, null);
        HtmlTextView htmlTextView = dialogView.findViewById(R.id.html_textview);
        htmlTextView.setHtml(content, new HtmlAssetsImageGetter(htmlTextView));

        ImageView btnCancel = dialogView.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private View.OnClickListener shareListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");

                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_content) + " " + getString(R.string.store_url));
                    Intent chooser = Intent.createChooser(intent, getString(R.string.settings_share));
                    startActivity(chooser);
                }
            }, 100);
        }
    };

    private View.OnClickListener feedbackListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(context, FeedbackActivity.class));
                }
            }, 100);
        }
    };

    private View.OnClickListener noAdsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!storage.purchasedRemoveAds()) {
                        bp.purchase(SettingsActivity.this, Config.NO_ADS_SKU);
                    }
                }
            }, 100);
        }
    };
    private View.OnClickListener restoreListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            bp.loadOwnedPurchasesFromGoogle();
        }
    };


    private View.OnClickListener reviewListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            final String appPackageName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    };

    private View.OnClickListener termsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showTermsDialog();
                }
            }, 100);
        }
    };

    private View.OnClickListener wishpokeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Config.WISHPOKE_GP_URL)));
        }
    };


    private void showTermsDialog() {
        if (dialog == null) {
            dialog = new AlertDialog.Builder(this)
                    .setView(dialogView)
                    .create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
        dialog.show();
    }

    @Override
    protected void onPause() {
        storage.setQuitAt();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (storage.isReconnection() && !storage.purchasedRemoveAds()) {
            interstitialAd.show();
        }
        super.onResume();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        if (productId.equals(Config.NO_ADS_SKU) &&
                Objects.requireNonNull(details).purchaseInfo.purchaseData.purchaseState == PurchaseState.PurchasedSuccessfully) {
            storage.setPurchasedRemoveAds(bp.isPurchased(Config.NO_ADS_SKU));
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {
        storage.setPurchasedRemoveAds(bp.isPurchased(Config.NO_ADS_SKU));
        if (!bp.isPurchased(Config.NO_ADS_SKU)) {
            Snackbar.make(tvNoAds, R.string.no_purchase_history, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        if (errorCode != Constants.BILLING_RESPONSE_RESULT_USER_CANCELED) {
            Snackbar.make(tvNoAds, R.string.error_unknown, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBillingInitialized() {
        storage.setPurchasedRemoveAds(bp.isPurchased(Config.NO_ADS_SKU));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }
}
