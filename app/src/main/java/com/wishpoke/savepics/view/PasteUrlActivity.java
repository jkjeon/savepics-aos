package com.wishpoke.savepics.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.wishpoke.savepics.R;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.util.Animator;
import com.wishpoke.savepics.util.Clipboard;
import com.wishpoke.savepics.util.Validator;
import com.wishpoke.savepics.view.adapter.RecentAdapter;
import com.wishpoke.savepics.viewmodel.PasteUrlViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import spencerstudios.com.bungeelib.Bungee;

public class PasteUrlActivity extends AppCompatActivity {

    @BindView(R.id.et_paste)
    EditText etPaste;
    @BindView(R.id.btn_go)
    LinearLayout btnGo;
    @BindView(R.id.tv_go)
    TextView tvGo;
    @BindView(R.id.rv_recent)
    RecyclerView rvRecent;
    @BindView(R.id.btn_back)
    ImageView btnBack;

    private Context context;
    private PasteUrlViewModel viewModel;
    private RecentAdapter adapter;
    private AppStorage storage;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paste_url);
        ButterKnife.bind(this);
        storage = new AppStorage(this);
        context = this;

        loadAds();

        viewModel = ViewModelProviders.of(this).get(PasteUrlViewModel.class);

        // Recent List 설정
        adapter = new RecentAdapter(viewModel, recentUrlClickListener);
        LinearLayoutManager reversedManager = new LinearLayoutManager(this);
        reversedManager.setReverseLayout(true);
        reversedManager.setStackFromEnd(true);

        rvRecent.setLayoutManager(reversedManager);
        rvRecent.setHasFixedSize(true);
        rvRecent.setAdapter(adapter);

        etPaste.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    btnGo.callOnClick();
                    return true;
                }
                return false;
            }
        });

        btnGo.setOnClickListener(btnGoClickListener);
        btnBack.setOnClickListener(btnBackClickListener);

        viewModel.getRecent().observe(this, new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(@Nullable ArrayList<String> urls) {
                adapter.setUrls(urls);
            }
        });
    }

    private View.OnClickListener recentUrlClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if (interstitialAd.isLoaded()) {
                interstitialAd.show();
                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        String url = (String) v.getTag();
                        etPaste.setText(url);
                        go(url);
                    }
                });
            } else {
                String url = (String) v.getTag();
                etPaste.setText(url);
                go(url);
            }
        }
    };

    private View.OnClickListener btnGoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (interstitialAd.isLoaded()) {
                interstitialAd.show();
                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        go();
                    }
                });
            } else {
                go();
            }
        }
    };

    private void go() {
        final String url = etPaste.getText().toString();
        tvGo.startAnimation(Animator.getClickAnimation(context));
        tvGo.postDelayed(new Runnable() {
            @Override
            public void run() {
                go(url);
                viewModel.addRecent(Validator.validateUrl(url));
            }
        }, 80);
    }

    private View.OnClickListener btnBackClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(Animator.getClickAnimation(context));
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onBackPressed();
                    Bungee.fade(context);
                }
            }, 80);
        }
    };

    private void go(String url) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    private void loadAds() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    @Override
    protected void onPause() {
        storage.setQuitAt();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (storage.isReconnection() && !storage.purchasedRemoveAds()) {
            interstitialAd.show();
        }
        String clipboard = Clipboard.readFromClipboard(this);
        if (clipboard != null) {
            etPaste.setText(clipboard);
        }
        super.onResume();
    }
}
