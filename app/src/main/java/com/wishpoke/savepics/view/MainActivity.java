package com.wishpoke.savepics.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.ImageAssetDelegate;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieImageAsset;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.facebook.FacebookSdk;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.FirebaseApp;
import com.wishpoke.savepics.R;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.repository.AppRepository;
import com.wishpoke.savepics.resource.Config;
import com.wishpoke.savepics.resource.FadePageTransformer;
import com.wishpoke.savepics.util.Animator;
import com.wishpoke.savepics.util.Clipboard;
import com.wishpoke.savepics.util.Validator;
import com.wishpoke.savepics.view.adapter.ViewPagerAdapter;
import com.wishpoke.savepics.viewmodel.PasteUrlViewModel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

public class MainActivity extends AppCompatActivity  implements BillingProcessor.IBillingHandler {

    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.adView)
    AdView adView;
    @BindView(R.id.et_paste)
    EditText etPaste;
    @BindView(R.id.btn_go)
    LinearLayout btnGo;
    @BindView(R.id.tv_go)
    TextView tvGo;
    @BindView(R.id.btn_login)
    LinearLayout btnLogin;
    @BindView(R.id.tv_login)
    TextView tvLogin;

    private Context context;
    private AlertDialog dialog;
    private AppStorage storage;
    private BillingProcessor bp;
    private InterstitialAd interstitialAd;
    private PasteUrlViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        viewModel = ViewModelProviders.of(this).get(PasteUrlViewModel.class);
        storage = new AppStorage(this);
        bp = new BillingProcessor(this, Config.GP_LICENSE_KEY, this);
        bp.initialize();
        bp.loadOwnedPurchasesFromGoogle();
        context = this;

        loadAds();

        if (storage.isFirstLaunch()) {
            showWarningDialog();
        }

        animationView.setImageAssetsFolder("images/");
        animationView.setAnimation("background.json");
        animationView.playAnimation();
    }

    private void loadAds() {
        final AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        interstitialAd.loadAd(adRequest );
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(adRequest);
            }
        });
    }

    public InterstitialAd getInterstitialAd() {
        return interstitialAd;
    }


    private void showWarningDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_warning, null);
        Button btnAgree = dialogView.findViewById(R.id.btn_agree);
        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = new AlertDialog.Builder(this)
                .setView(dialogView)
                .setCancelable(false)
                .create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }


    @OnClick(R.id.et_paste)
    public void onClickUrlInput(View v) {
        Intent intent = new Intent(context, PasteUrlActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    @OnClick(R.id.ic_settings)
    public void onClickSettings(View v) {
        v.startAnimation(Animator.getClickAnimation(context));
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(context, SettingsActivity.class));
                Bungee.slideLeft(context);
            }
        }, 100);
    }

    @OnClick(R.id.btn_go)
    public void onClickGo(View v) {
        tvGo.startAnimation(Animator.getClickAnimation(context));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                go();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
            go();
        }
    }

    @OnClick(R.id.btn_login)
    public void onClickLogin(View v) {
        tvLogin.startAnimation(Animator.getClickAnimation(context));
        tvLogin.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("url", Config.IG_URL + "accounts/login");
                intent.putExtra("is_regram", true);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);
            }
        }, 50);
    }

    private void go() {
        final String url = etPaste.getText().toString();
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
        viewModel.addRecent(Validator.validateUrl(url));
    }

    @Override
    protected void onPause() {
        storage.setQuitAt();
        adView.pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        bp.loadOwnedPurchasesFromGoogle();
        toggleAds();
        if (storage.isReconnection() && !storage.purchasedRemoveAds()) {
            interstitialAd.show();
        }
        super.onResume();
        String clipboard = Clipboard.readFromClipboard(context);
        if (clipboard != null) {
            etPaste.setText(clipboard);
        }
        adView.resume();
    }

    private void toggleAds() {
        if (storage.purchasedRemoveAds()) {
            adView.setVisibility(View.GONE);
        } else {
            adView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {
        storage.setPurchasedRemoveAds(bp.isPurchased(Config.NO_ADS_SKU));
        toggleAds();
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {
        storage.setPurchasedRemoveAds(bp.isPurchased(Config.NO_ADS_SKU));
        toggleAds();
    }


}
