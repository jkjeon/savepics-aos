package com.wishpoke.savepics.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.wishpoke.savepics.R;
import com.wishpoke.savepics.model.AppStorage;
import com.wishpoke.savepics.view.adapter.PhotoViewPagerAdapter;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaveCompleteActivity extends AppCompatActivity {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.btn_share_photo)
    Button btnSharePhoto;
    @BindView(R.id.btn_share)
    Button btnShare;
    @BindView(R.id.btn_main)
    Button btnMain;
    @BindView(R.id.tv_desc)
    TextView tvDesc;

    private Context context;
    private ArrayList<String> imageUrls = new ArrayList<>();
    private ArrayList<Uri> savedUris = new ArrayList<>();
    private PhotoViewPagerAdapter adapter;
    private AppStorage storage;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_complete);
        ButterKnife.bind(this);
        storage = new AppStorage(this);
        context = this;

        loadAds();

        imageUrls = getIntent().getStringArrayListExtra("images");
        savedUris = getIntent().getParcelableArrayListExtra("uris");

        String desc = tvDesc.getText().toString();
        desc = desc.replaceAll("##", String.valueOf(imageUrls.size()));
        tvDesc.setText(desc);

        adapter = new PhotoViewPagerAdapter(this);
        adapter.setImageUrls(imageUrls);
        pager.setAdapter(adapter);

        btnSharePhoto.setOnClickListener(sharePhotoListener);
        btnShare.setOnClickListener(shareListener);
        btnMain.setOnClickListener(goToMainListener);
    }

    private View.OnClickListener sharePhotoListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                Uri currentImageUri = FileProvider.getUriForFile(context, getPackageName() + ".provider",
                        new File(savedUris.get(pager.getCurrentItem()).getPath()));
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("image/png");

                intent.putExtra(Intent.EXTRA_STREAM, currentImageUri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Intent chooser = Intent.createChooser(intent, getString(R.string.share));
                startActivity(chooser);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    };

    private View.OnClickListener shareListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");

            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_content) + " " + getString(R.string.store_url));
            Intent chooser = Intent.createChooser(intent, getString(R.string.settings_share));
            startActivity(chooser);
        }
    };

    private View.OnClickListener goToMainListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (interstitialAd.isLoaded()) {
                interstitialAd.show();
                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        goToMain();
                    }
                });
            } else {
                goToMain();
            }
        }
    };

    private void goToMain() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void loadAds() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    @Override
    protected void onPause() {
        storage.setQuitAt();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (storage.isReconnection() && !storage.purchasedRemoveAds()) {
            interstitialAd.show();
        }
        super.onResume();
    }
}
