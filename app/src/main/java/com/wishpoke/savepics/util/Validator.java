package com.wishpoke.savepics.util;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private static final String googleQuery = "https://www.google.com/search?q=";

    public static boolean isUrl(String text) {
        Pattern p = Pattern.compile("^(?:https?:\\/\\/)?(?:www\\.)?[a-zA-Z0-9./]+$");
        Matcher m = p.matcher(text);
        if  (m.matches()) return true;
        URL u = null;
        try {
            u = new URL(text);
        } catch (MalformedURLException e) {
            return false;
        }
        try {
            u.toURI();
        } catch (URISyntaxException e) {
            return false;
        }
        return true;
    }

    public static String validateUrl(String url) {
        if (Validator.isUrl(url)) {
            if (!url.contains("http://") && !url.contains("https://")) {
                url = "http://" + url;
            }
        } else {
            url = googleQuery + url;
        }
        return url;
    }
}
