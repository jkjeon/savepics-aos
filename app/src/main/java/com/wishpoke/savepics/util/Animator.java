package com.wishpoke.savepics.util;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.wishpoke.savepics.R;
import com.wishpoke.savepics.resource.animation.BounceInterpolator;

public class Animator {
    public static Animation getClickAnimation(Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.bounce);
        BounceInterpolator interpolator = new BounceInterpolator();
        animation.setInterpolator(interpolator);
        return animation;
    }

    public static Animation getLoadingAnimation(Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.rotate);
        BounceInterpolator interpolator = new BounceInterpolator();
        animation.setInterpolator(interpolator);
        return animation;
    }
}
