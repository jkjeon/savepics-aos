package com.wishpoke.savepics.util;

import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;

public class Clipboard {
    public static String readFromClipboard(Context context) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip()) {
            android.content.ClipDescription description = clipboard.getPrimaryClipDescription();
            android.content.ClipData data = clipboard.getPrimaryClip();
            if (data != null && description != null && description.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                String result = data.getItemAt(0).getText().toString();
                if (Validator.isUrl(result)) {
                    Log.d("test", "isUrl: " + result);
                    return result;
                } else {
                    Log.d("test", "isNotUrl: " + result);
                }
            }
        }
        return null;
    }
}
