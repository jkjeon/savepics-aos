package com.wishpoke.savepics.util;

public interface MyCallback<T> {
    void done(T t);
}
