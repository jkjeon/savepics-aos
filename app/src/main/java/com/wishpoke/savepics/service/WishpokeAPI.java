package com.wishpoke.savepics.service;

import com.wishpoke.savepics.model.WishpokeRequestResult;

import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface WishpokeAPI {
    @Headers({ "Accept: application/json" })
    @POST("/common/getTerms")
    Single<WishpokeRequestResult> getTerms(@Body RequestBody body);
}

