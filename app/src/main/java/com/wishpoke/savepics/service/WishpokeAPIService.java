package com.wishpoke.savepics.service;

import com.wishpoke.savepics.resource.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class WishpokeAPIService {
    public static WishpokeAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Config.WISHPOKE_BASE_URL)
                .build();
        return retrofit.create(WishpokeAPI.class);
    }

    public static RequestBody getRequestBodyByKeyValuePair(HashMap<String, ?> pair) {
        JSONObject requestObject = new JSONObject();
        try {
            for (String key : pair.keySet()) {
                requestObject.put(key, pair.get(key));
            }
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),
                    requestObject.toString());
            return requestBody;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}