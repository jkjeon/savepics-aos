package com.wishpoke.savepics.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.wishpoke.savepics.repository.AppRepository;
import com.wishpoke.savepics.util.MyCallback;

import java.io.File;
import java.util.ArrayList;

public class SelectPhotoViewModel extends AndroidViewModel {
    private AppRepository repository;
    public SelectPhotoViewModel(@NonNull Application application) {
        super(application);
        repository = new AppRepository(application);
    }

    public void saveImages(ArrayList<String> imageUrls, MyCallback<ArrayList<Uri>>  callback) {
        repository.saveImages(imageUrls, callback);
    }
}
