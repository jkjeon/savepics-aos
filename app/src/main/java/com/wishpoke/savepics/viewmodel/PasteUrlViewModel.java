package com.wishpoke.savepics.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.wishpoke.savepics.repository.AppRepository;

import java.util.ArrayList;

public class PasteUrlViewModel extends AndroidViewModel {
    private AppRepository repository;
    public PasteUrlViewModel(@NonNull Application application) {
        super(application);
        repository = new AppRepository(application);
    }

    public void addRecent(String url) {
        repository.addRecent(url);
    }

    public void removeRecent(String url) {
        repository.removeRecent(url);
    }

    public MutableLiveData<ArrayList<String>> getRecent() {
        return repository.getRecent();
    }
}
