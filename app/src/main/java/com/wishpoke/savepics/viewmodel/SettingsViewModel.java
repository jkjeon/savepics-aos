package com.wishpoke.savepics.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.wishpoke.savepics.model.WishpokeRequestResult;
import com.wishpoke.savepics.repository.SettingsRepository;

import io.reactivex.Single;

public class SettingsViewModel extends AndroidViewModel {
    private SettingsRepository repository;
    public SettingsViewModel(@NonNull Application application) {
        super(application);
        repository = new SettingsRepository();
    }

    public Single<WishpokeRequestResult> getTerms() {
        return repository.getTerms();
    }
}
