package com.wishpoke.savepics.resource;

public class Config {
    public static final String FEEDBACK_URL = "https://docs.google.com/forms/d/e/1FAIpQLSfZEdWfkMBNG7wuEmcQLHmGxPVggYVl3LKSe7clcjmPXYIf-A/viewform";
    public static final String WISHPOKE_BASE_URL = "http://api.wishpoke.com";
    public static final String WISHPOKE_GP_URL = "https://play.google.com/store/apps/dev?id=6952801736307439575&hl=ko";
    public static final String NO_ADS_SKU = "no_ads";
    public static final String GP_LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk0i/VYpHTVMv6Vx2qO+wwV4O6WzGI8QBrthncWKXKRCTl0WB07X8vUny0SAB/Jv/PKupzyJiJjuAxOuZi2AxF/7Jgw223qhtry0mCr7yvfldarB1AZJGM2uedfwP9cu8uPhjwOZ5qCWw9eEhx/XHwy+8s2eVpHpQ/AoBVT/X8r1uiKEXX2eo3wM8E91mLRStPEaElL6fMlJSWLrHyJ6okLqzMTjCcTupPfBXrzgWgH+7taVTj9dheEIdr1NgGt6Zhxc1VkFZetZwGZAwNtcEERYC7CnFkIoBgm1ZgBHjjRzGk64F/ceNLFWtFTN8qbPUJf9H2QDyGvOIkKKn0v+sPwIDAQAB";
    public final static String IG_URL = "https://www.instagram.com/";
}
